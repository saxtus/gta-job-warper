# GTA job warper

AutoHotKey script that allows you to automate the task of job warping in GTA using a hotkey

## Usage

- Point your map to the job you need and press F8
- Press CTRL+F8 if the script stuck spamming mouse, to restart it
- use --exit command line argument if you have some automation and want to quit the script
- use --debug to write a log file

## History

### v2.2

2022-07-10
- Replaced ESC with CTRL+F8 for unstucking the script

### v2.1

2022-07-10
- Added --debug command line
- Now it reads pixel from 500, 100 to avoid any conflicts with overlays at corners
- Tidy up some variables

### v2.0

2022-07-09
- It can now detect the cancel prompt and act accordingly

### v1.0

2022-07-08
- First release
