; GTA job warper v2.2
; Usage:
; Point your map to the job you need and press F8
; Press CTRL+F8 if the script stuck spammin the mouse, to restart it
; use --exit command line argument if you have some automation and want to quit the script
; use --debug to write log file

#SingleInstance force
#Persistent

If A_Args[1] == "--exit"
ExitApp

If A_Args[1] == "--debug"
global dBug_active := true

debug(string) {
   if (dBug_active) {
      FileAppend, % A_NowUTC ": " string "`n", gta job warper.txt
   }
}
F8::
debug("F8 clicked")
wait_iterations = 20
global BreakLoop := false
slow_fade := false
Send {Space}{Enter}
debug("Sent Space+Enter")
Loop
{
   if (BreakLoop) { ; Another thread signaled us to stop
      debug("Another thread signaled us to stop")
      break
   }
   if (A_Index >= wait_iterations) {
      PixelGetColor, color, 500,100
      debug("Color: " color ", slow_fade:" slow_fade ", A_Index: " A_Index)
      If (color = 0x000000) {
         if (A_Index = wait_iterations) { ; Initiated through interior, thus we got a slow fade to black instead of clouds, let's wait for it to end
            debug("Slow fade detected")
            slow_fade := true
         }
         else {
            if (!slow_fade) { ; We got the cancel prompt, time to finish the mouse spamming and accept it
               debug("Cancel prompt detected")
               BreakLoop := true
               Send {Enter}
               debug("Stopped spamming mouse and sent enter")
               break
            }
         }
      }
      else {
         if (slow_fade) { ; Slow fade seems to have ended, time to start the actual black prompt detection
            debug("Slow fade ended")
            slow_fade := false
         }
      }
   }
   MouseClick, right
   Sleep, 100

}
return

^F8::BreakLoop := true
